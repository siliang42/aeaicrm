package com.agileai.crm.module.mytasks.handler;

import com.agileai.crm.cxmodule.FollowUpManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.MasterSubEditPboxHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.StringUtil;

public class MyCustVisitInfoHandler
        extends MasterSubEditPboxHandler {
    public MyCustVisitInfoHandler() {
        super();
        this.serviceId = buildServiceId(FollowUpManage.class);
        this.subTableId = "MyCustVisitInfo";
    }
    
	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getSubRecord(subTableId, param);
			if(record == null){
				record = getService().getCustVisitInfoRecord(param);
			}
			this.setAttributes(record);			
		}
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

    protected void processPageAttributes(DataParam param) {
        if (!StringUtil.isNullOrEmpty(param.get("TASK_ID"))) {
            this.setAttribute("TASK_ID", param.get("TASK_ID"));
        }
        setAttribute("PROCUST_VISIT_EFFECT",
                FormSelectFactory.create("VISIT_EFFECT")
                                 .addSelectedValue(getAttributeValue("PROCUST_VISIT_EFFECT",
                                                                          "")));
	    setAttribute("PROCUST_VISIT_TYPE",
	           FormSelectFactory.create("VISIT_TYPE")
	                            .addSelectedValue(getAttributeValue("PROCUST_VISIT_TYPE",
	                                                                     "")));
    }

    protected FollowUpManage getService() {
        return (FollowUpManage) this.lookupService(this.getServiceId());
    }
}
