package com.agileai.crm.module.procustomer.service;

import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.crm.cxmodule.OrgInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class OrgInfoManageImpl
        extends StandardServiceImpl
        implements OrgInfoManage {
    public OrgInfoManageImpl() {
        super();
    }
    
	public void createVisitRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"createVisitRecord";
		String visitId = KeyGenerator.instance().genKey();
   		param.put("VISIT_ID",visitId);
		processDataType(param, tableName);
		processPrimaryKeys(param);
		this.daoHelper.insertRecord(statementId, param);
	}
	
	@Override
	public void createCustomerRecord(DataParam param,String custId) {
		String statementId = sqlNameSpace+"."+"createCustomerRecord";
   		param.put("CUST_ID",custId);
		processDataType(param, tableName);
		processPrimaryKeys(param);
		this.daoHelper.insertRecord(statementId, param);
		
		statementId = sqlNameSpace+"."+"getGrpCodeRecord";
		param.put("GRP_CODE","TEMP");
		DataRow result = this.daoHelper.getRecord(statementId, param);
		String grpId = result.getString("GRP_ID");
		statementId = sqlNameSpace+"."+"createCustomerGrpRecord";
		DataParam newParam = new DataParam();
		newParam.put("GRP_ID",grpId);
		newParam.put("CUST_ID",param.get("CUST_ID"));
		this.daoHelper.insertRecord(statementId, newParam);
	}
	
	@Override
	public void changeStateRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"changeStateRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
	}
	
	@Override
	public List<DataRow> findCustomerRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findCustomerRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public List<DataRow> findOrgLabelsRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findOrgLabelsRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public void addSalesManRelation(DataParam param) {
		String statementId = sqlNameSpace+".addUserTreeRelation";
		this.daoHelper.insertRecord(statementId, param);
	}

	@Override
	public void assignedSaleRecord(DataParam param) {
		String statementId = sqlNameSpace+".assignedSaleRecord";
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public List<DataRow> findOrgSalesmanRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findOrgSalesmanRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public void batchDeleteRecords(List<DataParam> paramList) {
		String statementId = sqlNameSpace+".deleteRecord";
		this.daoHelper.batchDelete(statementId, paramList);
	}

	@Override
	public void batchCreateRecord(List<DataParam> paramList) {
		String statementId = sqlNameSpace+".insertRecord";
		this.daoHelper.batchInsert(statementId, paramList);
	}
}
