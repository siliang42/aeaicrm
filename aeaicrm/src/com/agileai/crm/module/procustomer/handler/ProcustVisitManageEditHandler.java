package com.agileai.crm.module.procustomer.handler;

import com.agileai.crm.cxmodule.ProcustVisitManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class ProcustVisitManageEditHandler
        extends StandardEditHandler {
    public ProcustVisitManageEditHandler() {
        super();
        this.listHandlerClass = ProcustVisitManageListHandler.class;
        this.serviceId = buildServiceId(ProcustVisitManage.class);
    }
    
	public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		String custVisitCategory = param.get("CUST_VISIT_CATEGORY");
		if("PRO_CUST".equals(custVisitCategory)){
			DataRow record = getService().getRecord(param);
			this.setAttributes(record);
		}else if("FOLLOW_CUST".equals(custVisitCategory)){
			DataRow record = getService().getCustRecord(param);
			this.setAttributes(record);
		}
		this.setOperaType(operaType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

    protected void processPageAttributes(DataParam param) {
        setAttribute("CUST_VISIT_CATEGORY",
                FormSelectFactory.create("CUST_VISIT_CATEGORY")
                                 .addSelectedValue(getOperaAttributeValue("CUST_VISIT_CATEGORY",
                                                                          "")));
        setAttribute("PROCUST_VISIT_EFFECT",
                     FormSelectFactory.create("VISIT_EFFECT")
                                      .addSelectedValue(getOperaAttributeValue("PROCUST_VISIT_EFFECT",
                                                                               "")));
        setAttribute("PROCUST_VISIT_TYPE",
                FormSelectFactory.create("VISIT_TYPE")
                                 .addSelectedValue(getOperaAttributeValue("PROCUST_VISIT_TYPE",
                                                                          "")));
    }

    protected ProcustVisitManage getService() {
        return (ProcustVisitManage) this.lookupService(this.getServiceId());
    }
}
