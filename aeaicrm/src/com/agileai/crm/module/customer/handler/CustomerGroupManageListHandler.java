package com.agileai.crm.module.customer.handler;

import java.util.ArrayList;
import java.util.List;

import com.agileai.crm.module.customer.service.CustomerGroupManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;
import com.agileai.hotweb.controller.core.TreeAndContentManageListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.hotweb.domain.TreeModel;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class CustomerGroupManageListHandler
        extends TreeAndContentManageListHandler {
    public CustomerGroupManageListHandler() {
        super();
        this.serviceId = buildServiceId(CustomerGroupManage.class);
        this.rootColumnId = "00000000-0000-0000-00000000000000000";
        this.defaultTabId = CustomerGroupManage.BASE_TAB_ID;
        this.columnIdField = "GRP_ID";
     	this.columnNameField = "GRP_NAME";
    	this.columnParentIdField = "GRP_SUP_ID";
    	this.columnSortField = "GRP_SORT";
    }
    
    public ViewRenderer prepareDisplay(DataParam param){
		String columnId = param.get("columnId",this.rootColumnId);
		this.setAttribute("columnId", columnId);
		this.setAttribute("isRootColumnId",String.valueOf(this.rootColumnId.equals(columnId)));
		
		TreeBuilder treeBuilder = provideTreeBuilder(param);
		TreeModel treeModel = treeBuilder.buildTreeModel();
		
		String menuTreeSyntax = this.getTreeSyntax(treeModel,new StringBuffer());
		this.setAttribute("menuTreeSyntax", menuTreeSyntax);
		String tabId = param.get(TreeAndContentManage.TAB_ID,this.defaultTabId);
		
		DataParam queryParam = new DataParam(columnIdField,columnId);
		DataRow row = getService().queryTreeRecord(queryParam);
		this.setAttributes(row);
		
		this.setAttribute(TreeAndContentManage.TAB_ID, tabId);
		this.setAttribute(TreeAndContentManage.TAB_INDEX, getTabIndex(tabId));
		
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
    protected void processPageAttributes(DataParam param) {
        String tabId = param.get(TreeAndContentManage.TAB_ID, this.defaultTabId);

        if ("CustomerInfo".equals(tabId)) {
        }
		 setAttribute("GRP_STATE",
                 FormSelectFactory.create("GRP_STATE")
                                  .addSelectedValue(getOperaAttributeValue("GRP_STATE",
                                                                           "")));
		 setAttribute("CHILD_GRP_STATE",
                 FormSelectFactory.create("GRP_STATE")
                                  .addSelectedValue(getAttributeValue("CHILD_GRP_STATE",
                                                                           "1")));
    }

    protected void initParameters(DataParam param) {
        String tabId = param.get(TreeAndContentManage.TAB_ID, this.defaultTabId);

        if ("CustomerInfo".equals(tabId)) {
        	  
        }
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        CustomerGroupManage service = this.getService();
        List<DataRow> menuRecords = service.findTreeGroupRecords(new DataParam());
        TreeBuilder treeBuilder = new TreeBuilder(menuRecords,
                                                  this.columnIdField,
                                                  this.columnNameField,
                                                  this.columnParentIdField);
        return treeBuilder;
    }

    protected List<String> getTabList() {
        List<String> result = new ArrayList<String>();
        result.add(CustomerGroupManage.BASE_TAB_ID);
        result.add("CustomerInfo");

        return result;
    }

	public ViewRenderer doInsertChildAction(DataParam param){
		DataParam dataParam = new DataParam("GRP_CODE",param.get("CHILD_GRP_CODE"));
		DataRow personRecords = getService().getGroupRecord(dataParam);
		if (personRecords!=null){
			this.setErrorMsg("分组编码已存在,不能新增!");
			return prepareDisplay(param);
		}
		getService().insertChildRecord(param);
		return prepareDisplay(param);
	}
	
    public ViewRenderer doSaveTreeBaseRecordAction(DataParam param){
    	String rspText = FAIL;
    	try {
    		getService().updateTreeRecord(param);
    		rspText = SUCCESS;
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
    	return new AjaxRenderer(rspText);
    }
    protected int getTabIndex(String curTabId) {
		int result = 0;
		List<String> temp = getTabList();
		for (int i=0;i < temp.size();i++){
			if (curTabId.equals(temp.get(i))){
				result = i;
				break;
			}
		}
		return result;
	}
    
   
    public ViewRenderer doRefreshAction(DataParam param){
		return prepareDisplay(param);
	}
    
	public ViewRenderer doMoveUpAction(DataParam param){
		String rspText = SUCCESS;
		TreeAndContentManage service = this.getService();
		String columnId = param.get("columnId");
		if (service.isFirstTreeChild(columnId)){
			rspText = "isFirstNode";
		}else{
			service.changeTreeSort(columnId, true);			
		}
		return new AjaxRenderer(rspText);
	}
	public ViewRenderer doMoveDownAction(DataParam param){
		String rspText = SUCCESS;
		TreeAndContentManage service = this.getService();
		String columnId = param.get("columnId");
		if (service.isLastTreeChild(columnId)){
			rspText = "isLastNode";
		}else{
			service.changeTreeSort(columnId, false);
		}
		return new AjaxRenderer(rspText);
	}
	
    protected CustomerGroupManage getService() {
        return (CustomerGroupManage) this.lookupService(this.getServiceId());
    }
}
