package com.agileai.crm.cxmodule;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;

public interface CustomerGroup8ContentManage extends TreeAndContentManage {
	public String[] getTableIds();

	public List<DataRow> findSubRecords(String subId, DataParam param);

	public DataRow getCustomerInfoRecord(DataParam param);

	public void saveSubRecords(DataParam param, List<DataParam> insertRecords, List<DataParam> updateRecords);
	
	public void  updatetConfirmRecord(String tabId,DataParam param);
	
	public void deleteSubRecord(String subId, DataParam param);
	
	public List<DataRow> findRecords(DataParam param);
	
	public void updateSubmitStateInfoRecord(DataParam param);
	
	public void updateConfirmStateInfoRecord(DataParam param);
	
	public void addUserTreeRelation(String custId,String[] userIds);
	
	public List<DataRow> findSalesManRelation(DataParam param);
	
	public void addSalesManRelation(DataParam param);
	
	public void delUserTreeRelation(String custId,String userId);
	
	public List<DataRow> findCityRecords(String proviceCode);
	
	public DataRow getStatisticsRecords(DataParam param);
	
	public List<DataRow> findPersonRecords(DataParam param);
	
	public List<DataRow> findSalesRecords(DataParam param);
	
	public List<DataRow> findMyCustomersList(DataParam param);
	
	public DataRow getCustomersInfo(DataParam param);
	
	public void createCustomers(DataParam param);
	
	public void editCustomersDetails(DataParam param);
	
	public void deletContentsInfo(DataParam param);
	
	public void deleteCustomersInfo(DataParam param);
	
	public List<DataRow> findCustomersVisitInfo(DataParam param);
	
	public DataRow getCustomersVisitInfo(DataParam param);
	
	public void createCustomersVisitInfo(DataParam param);
	
	public void editCustomersVisitInfo(DataParam param);
	
	public void deleteCustomersVisitInfo(DataParam param);
	
	public List<DataRow> findCustomersOppInfo(DataParam param);
	
	public DataRow getCustomersOppInfo(DataParam param);
	
	public void createCustomersOppInfo(DataParam param);
	
	public void editCustomersOppInfo(DataParam param);
	
	public void deleteCustomersOppInfo(DataParam param);
	
	public DataRow getCustomersContInfo(DataParam param);
	
	public void createCustomersContactsInfo(DataParam param);
	
	public void editCustomersContactsInfo(DataParam param);
	
	public void deleteCustomersContactsInfo(DataParam param);
	
	public List<DataRow> findCustomersOrderInfo(DataParam param);

	public List<DataRow> getStatInfo();

	public List<DataRow> getOppContentData(String date);

	public List<DataRow> getOrderContentData(String date);

	public List<DataRow> getOrderSubContentData(String id);

	public List<DataRow> findSalesList(DataParam param);
	
	public DataRow queryTempGrpId(DataParam param);
	
	public void insertCustomerInfoRelation(DataParam param);
	
	public List<DataRow> findMyCustomerRecords(DataParam param);
	
	public List<DataRow> findSaleManRecords(DataParam param);
	
	public List<DataRow> findProgressStateRecords(DataParam param);
	
	List<DataRow> findSaleTreeRecords(DataParam param);
	
	public void updateStateInfoRecord(DataParam param);
	
	List<DataRow> findCustVisitInfoRecords(DataParam param);
	
	void updateVisitCustIdRecord(DataParam param);
	
	DataRow getContforVisitRecord(DataParam param);
	
	List<DataRow> findOppOrderWeekRecords(DataParam param);
	
	List<DataRow> findOppOrderRecords(DataParam param);

}




