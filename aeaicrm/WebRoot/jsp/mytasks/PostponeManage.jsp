<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
function doSure(){
	var date = $('#PROCUST_VISIT_DATE').val();
	var type = $('#PROCUST_VISIT_TYPE').val();
	var effect = $('#PROCUST_VISIT_EFFECT').val();
	if(date == ''){
		writeErrorMsg('拜访日期不能为空');
		return;
	}else if(type == ''){
		writeErrorMsg('拜访类型不能为空');
		return;
	}else if(effect == ''){
		writeErrorMsg('拜访效果不能为空');
		return;
	}else{
		postRequest('form1',{actionType:'save',onComplete:function(rspText){
			if(rspText=='success'){
				parent.goBackId($('#TASK_REVIEW_ID').val());		
			}
		}});
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSure()"><input value="&nbsp;" type="button" class="saveImgBtn" id="followUpImgBtn" title="确定" />确定</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="B" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>   
</tr>
</table>
</div>
<div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>拜访日期</th>
	<td><input id="PROCUST_VISIT_DATE" label="拜访日期" name="PROCUST_VISIT_DATE" type="text" value="<%=pageBean.inputDate("PROCUST_VISIT_DATE")%>" size="24" class="text" readonly="readonly"/><img id="PROCUST_VISIT_DATEPicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
</td>
</tr>
<tr>
	<th width="100" nowrap>拜访类型</th>
    <td><select id="PROCUST_VISIT_TYPE" label="拜访类型" name="PROCUST_VISIT_TYPE" style="width: 303px" class="select"><%=pageBean.selectValue("PROCUST_VISIT_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>拜访效果</th>
    <td><select id="PROCUST_VISIT_EFFECT" label="拜访效果" name="PROCUST_VISIT_EFFECT" style="width: 303px" class="select"><%=pageBean.selectValue("PROCUST_VISIT_EFFECT")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>填写人</th>
	<td><input id="PROCUST_VISIT_FILL_NAME" label="填写人" name="PROCUST_VISIT_FILL_NAME" type="text" value="<%=pageBean.inputValue("PROCUST_VISIT_FILL_NAME")%>" size="24" class="text" readonly="readonly"/>
	<input id="PROCUST_VISIT_FILL_ID" label="填写人" name="PROCUST_VISIT_FILL_ID" type="hidden" value="<%=pageBean.inputValue("PROCUST_VISIT_FILL_ID")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>填写时间</th>
	<td><input id="PROCUST_VISIT_FILL_TIME" label="填写时间" name="PROCUST_VISIT_FILL_TIME" type="text" value="<%=pageBean.inputTime("PROCUST_VISIT_FILL_TIME")%>" size="24" class="text" readonly="readonly"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>拜访备注</th>
	<td><textarea id="PROCUST_VISIT_REMARK" label="拜访备注" name="PROCUST_VISIT_REMARK" cols="40" rows="3" class="textarea"><%=pageBean.inputValue("PROCUST_VISIT_REMARK")%></textarea>
</td>
</tr>
<tr>
	<th width="100" nowrap>客户关注点</th>
	<td><textarea id="PROCUST_VISIT_CUST_FOCUS" label="客户关注点" name="PROCUST_VISIT_CUST_FOCUS" cols="40" rows="3" class="textarea"><%=pageBean.inputValue("PROCUST_VISIT_CUST_FOCUS")%></textarea>
</td>
</tr>
</table>
</div>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="PROCUST_VISIT_ID" name="PROCUST_VISIT_ID" value="<%=pageBean.inputValue("PROCUST_VISIT_ID")%>" />
<input type="hidden" id="ORG_ID" name="ORG_ID" value="<%=pageBean.inputValue("ORG_ID")%>" />
<input type="hidden" id="CUST_ID" name="CUST_ID" value="<%=pageBean.inputValue("CUST_ID")%>" />
<input type="hidden" id="TASK_ID" name="TASK_ID" value="<%=pageBean.inputValue("TASK_ID")%>" />
<input type="hidden" name="TASK_REVIEW_ID" id="TASK_REVIEW_ID" value="<%=pageBean.inputValue("TASK_REVIEW_ID")%>" />
<input type="hidden" name="CUST_NAME" id="CUST_NAME" value="<%=pageBean.inputValue("CUST_NAME")%>" />
</form>
<script language="javascript">
initCalendar('PROCUST_VISIT_DATE','%Y-%m-%d','PROCUST_VISIT_DATEPicker');
initCalendar('PROCUST_VISIT_FILL_TIME','%Y-%m-%d %H:%M','PROCUST_VISIT_FILL_TIMEPicker');
datetimeValidators[0].set("yyyy-MM-dd").add("PROCUST_VISIT_DATE");
datetimeValidators[1].set("yyyy-MM-dd HH:mm").add("PROCUST_VISIT_FILL_TIME");
initDetailOpertionImage();
requiredValidator.add("PROCUST_VISIT_DATE");
requiredValidator.add("PROCUST_VISIT_TYPE");
requiredValidator.add("PROCUST_VISIT_EFFECT");
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
