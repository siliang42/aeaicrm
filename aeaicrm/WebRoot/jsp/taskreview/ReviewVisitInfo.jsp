<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveRecord(){
	if (!validate()){
		return;
	}
	showSplash();
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if (responseText == 'success'){
			hideSplash();
			parent.PopupBox.closeCurrent();
			parent.refreshPage();
		}
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>拜访日期</th>
	<td><input id="PROCUST_VISIT_DATE" label="拜访日期" name="PROCUST_VISIT_DATE" type="text" value="<%=pageBean.inputDate("PROCUST_VISIT_DATE")%>" size="24" class="text" readonly="readonly"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>拜访类型</th>
    <td><input id="PROCUST_VISIT_TYPE_TEXT" label="拜访类型" name="PROCUST_VISIT_TYPE_TEXT" type="text" value="<%=pageBean.selectedText("PROCUST_VISIT_TYPE")%>" size="24"  class="text" readonly="readonly"/>
	<input id="PROCUST_VISIT_TYPE" label="拜访类型" name="PROCUST_VISIT_TYPE" type="hidden" value="<%=pageBean.selectedValue("PROCUST_VISIT_TYPE")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>拜访效果</th>
    <td><input id="PROCUST_VISIT_EFFECT_TEXT" label="拜访效果" name="PROCUST_VISIT_EFFECT_TEXT" type="text" value="<%=pageBean.selectedText("PROCUST_VISIT_EFFECT")%>" size="24"  class="text" readonly="readonly"/>
	<input id="PROCUST_VISIT_EFFECT" label="拜访效果" name="PROCUST_VISIT_EFFECT" type="hidden" value="<%=pageBean.selectedValue("PROCUST_VISIT_EFFECT")%>" />
</td>
</tr>
<tr>
	<th width="100" nowrap>填写人</th>
	<td><input id="PROCUST_VISIT_FILL_NAME" label="填写人" name="PROCUST_VISIT_FILL_NAME" type="text" value="<%=pageBean.inputValue("PROCUST_VISIT_FILL_NAME")%>" size="24" class="text" readonly="readonly"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>填写时间</th>
	<td><input id="PROCUST_VISIT_FILL_TIME" label="填写时间" name="PROCUST_VISIT_FILL_TIME" type="text" value="<%=pageBean.inputTime("PROCUST_VISIT_FILL_TIME")%>" size="24" class="text" readonly="readonly"/>
</td>
</tr>
<tr>
	<th width="100" nowrap>拜访备注</th>
	<td><textarea id="PROCUST_VISIT_REMARK" label="拜访备注" name="PROCUST_VISIT_REMARK" cols="40" rows="3" class="textarea" readonly="readonly"><%=pageBean.inputValue("PROCUST_VISIT_REMARK")%></textarea>
</td>
</tr>
<tr>
	<th width="100" nowrap>客户关注点</th>
	<td><textarea id="PROCUST_VISIT_CUST_FOCUS" label="客户关注点" name="PROCUST_VISIT_CUST_FOCUS" cols="40" rows="3" class="textarea" readonly="readonly"><%=pageBean.inputValue("PROCUST_VISIT_CUST_FOCUS")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="CUST_ID" name="CUST_ID" value="<%=pageBean.inputValue4DetailOrUpdate("CUST_ID","")%>" />
<input type="hidden" id="CUST_NAME" name="CUST_NAME" value="<%=pageBean.inputValue("CUST_NAME")%>" />
</form>
<script language="javascript">
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
